const express = require('express');
const router = express.Router();
const { createProxyMiddleware } = require('http-proxy-middleware');

const MOBILE_API = 'https://mobileapi.finalsurge.com/';
const CLIENT_SECRET = 'U5ZR_N25H4U7wVKWubtZ5*Ny4X+2nJ';
const CLIENT_ID = 'd99d0dc7-60a2-42b3-bc6a-dc09935521b9';

router.use(
  '/',
  createProxyMiddleware({
    target: MOBILE_API,
    pathRewrite(path, req) {
      return path.replace(/api\/(Data|File)\?request=(\w*)&?(.*)/i, '$2?$3');
    },
    changeOrigin: true,
    proxyTimeout: 15000,
    timeout: 15000,
    onProxyReq(proxyReq, req, res) {
      proxyReq.setHeader('client-secret', CLIENT_SECRET);
      proxyReq.setHeader('client-id', CLIENT_ID);
    },
  })
);

module.exports = router;
