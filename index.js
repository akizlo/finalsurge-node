const express = require('express');
const compression = require('compression')
const api = require('./api.js');

const app = express();

const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 80;

app.use(compression());
app.use('/api', api);
app.use(express.static('static'));
app.use((req, res) => res.sendFile(`${__dirname}/static/index.html`));
app.listen(port, host);

console.log(`ENVIRONMENT => ${process.env.NODE_ENV}`);
console.log(`LISTENING => http://${host}:${port}`);
